Rails.application.routes.draw do
  resources :sessions
  resources :dashboards do
    collection do
      get :online
    end
  end

  namespace :research do
    resources :rtc_connections do
      collection do
        get :live_broadcast
        get :live_streaming
        post :synchronization
        post :broadcast
      end
    end

    resources :synchronization
  end

  namespace :web do
    resources :chats do
      collection do
        get  :load_room
        post :publish
      end
    end

    resources :groups
    resources :profiles
  end

  namespace :admin do
    resources :categories
  end

  namespace :api do
    resources :syncs, path: "sync" do
      collection do
        post :call
      end
    end

    resources :categories do
      collection do
        get :main_category
      end
    end
    
    resources :chats do
      collection do
        post :chatroom
        post :publish
        post :image
        post :video
        post :voice
        post :file
      end
    end

    resources :groups do
      collection do
        post :chatroom
        post :publish
      end
      
      member do
        post :register
        post :select
      end
    end
    
    resources :users do
      collection do
        post :member
        post :status
        post :resend
        get :otp
      end

      member do
        post :register
        post :device_token
        post :avatar
        get :contact
      end
    end

    resources :notes do
      collection do
        post :create
        post :story_like
        post :story_comment
        get :index
      end

      member do
        post :notes_group
      end
    end
  end

  resources :message_groups
  resources :messages
  resources :groups
  resources :users
  resources :conversations
  
  root "dashboards#index"
  mount ActionCable.server, at: '/cable'
end
