class NoteComment < ActiveRecord::Migration[5.2]
  def change
  	create_table :note_comments do |t|
      t.integer :note_id
      t.integer :user_id
      t.string :content

      t.timestamps
  	end

  	add_index :note_comments, :note_id
  	add_index :note_comments, :user_id
  end
end
