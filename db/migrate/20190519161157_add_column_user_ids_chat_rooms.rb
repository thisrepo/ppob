class AddColumnUserIdsChatRooms < ActiveRecord::Migration[5.2]
  def change
  	add_column :chat_rooms, :user_ids, :string
  	add_index :chat_rooms, :user_ids
  end
end
