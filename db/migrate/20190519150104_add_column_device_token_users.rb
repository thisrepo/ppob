class AddColumnDeviceTokenUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :device_token, :text
  	add_index :users, :device_token
  end
end
