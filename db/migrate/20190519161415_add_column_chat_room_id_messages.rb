class AddColumnChatRoomIdMessages < ActiveRecord::Migration[5.2]
  def change
  	remove_column :messages, :user_to
  	add_column :messages, :chat_room_id, :integer
  	add_index :messages, :chat_room_id
  end
end
