class AddColumnCategoryIdGroupsFix < ActiveRecord::Migration[5.2]
  def change
  	remove_column :categories, :category_id
  	add_column :groups, :category_id, :integer
  end
end
