class EditNoteLike < ActiveRecord::Migration[5.2]
  def change
    add_column :note_likes, :group_id, :integer
    add_column :note_comments, :group_id, :integer    
  end
end
