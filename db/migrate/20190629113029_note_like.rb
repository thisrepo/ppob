class NoteLike < ActiveRecord::Migration[5.2]
  def change
  	create_table :note_likes do |t|
      t.integer :note_id
      t.integer :user_id

      t.timestamps
  	end

  	add_index :note_likes, :note_id
  	add_index :note_likes, :user_id
  end
end
