class EditNotes < ActiveRecord::Migration[5.2]
  def change
  	remove_column :notes, :like
  	remove_column :notes, :comment
  end
end
