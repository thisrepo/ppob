class AddContentTypeToMessages < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :content_type, :string
  end
end
