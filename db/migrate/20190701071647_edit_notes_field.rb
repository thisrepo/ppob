class EditNotesField < ActiveRecord::Migration[5.2]
  def change
  	add_column :notes, :is_blast, :boolean, :default => false
  end
end
