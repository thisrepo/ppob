class AddContentTypeToMessageGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :message_groups, :content_type, :string
  end
end
