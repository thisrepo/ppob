class AddAttachmentVoiceToAttachments < ActiveRecord::Migration[5.2]
  def self.up
    change_table :attachments do |t|
      t.attachment :voice
    end
  end

  def self.down
    remove_attachment :attachments, :voice
  end
end
