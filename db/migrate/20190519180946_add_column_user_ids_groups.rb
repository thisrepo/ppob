class AddColumnUserIdsGroups < ActiveRecord::Migration[5.2]
  def change
  	change_column :groups, :parent_id, :integer, default: 0
  	add_column :groups, :user_ids, :string
  	add_column :groups, :premium, :boolean, default: false
  	add_index :groups, :user_ids
  end
end
