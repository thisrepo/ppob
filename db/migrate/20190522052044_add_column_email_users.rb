class AddColumnEmailUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :email, :string, default: "not set"
  end
end
