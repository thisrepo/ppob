class AddIsValidate < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :is_validate, :boolean, default: false
  end
end
