class CreateUserOtp < ActiveRecord::Migration[5.2]
  def change
    create_table :user_otps do |t|
      t.string :phone
      t.integer :otp

      t.timestamps
    end
  end
end
