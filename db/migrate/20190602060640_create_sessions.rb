class CreateSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.string :sign_id
      t.string :ip_address
      t.datetime :expired_in
      t.string :phone
      t.datetime :sign_at

      t.timestamps
    end

    add_index :sessions, :sign_id
    add_index :sessions, :phone
  end
end
