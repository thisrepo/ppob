class ModifyUserOtp < ActiveRecord::Migration[5.2]
  def change
  	drop_table :user_otps
  	add_column :users, :otp, :string
  end
end
