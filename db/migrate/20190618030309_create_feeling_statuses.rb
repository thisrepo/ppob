class CreateFeelingStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :feeling_statuses do |t|
      t.integer :user_id
      t.string :content

      t.timestamps
    end

    add_index :feeling_statuses, :content
  end
end
