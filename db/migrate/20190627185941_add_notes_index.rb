class AddNotesIndex < ActiveRecord::Migration[5.2]
  def change
  	add_index :notes, :group_id
  	add_index :notes, :user_id
  end
end
