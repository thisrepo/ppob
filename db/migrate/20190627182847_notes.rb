class Notes < ActiveRecord::Migration[5.2]
  def change
  	create_table :notes do |t|
      t.integer :group_id
      t.integer :user_id
      t.integer :like
      t.string :content
      t.string :comment
  	end
  end
end
