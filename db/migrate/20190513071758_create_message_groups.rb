class CreateMessageGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :message_groups do |t|
      t.integer :user_id
      t.integer :group_id
      t.text :content

      t.timestamps
    end

    add_index :message_groups, :user_id
    add_index :message_groups, :group_id
  end
end
