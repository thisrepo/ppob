class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :parent_id
      t.integer :user_id
      t.text :description

      t.timestamps
    end

    add_index :groups, :name
    add_index :groups, :parent_id
    add_index :groups, :user_id
  end
end
