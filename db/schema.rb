# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_01_071647) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachments", force: :cascade do |t|
    t.integer "message_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.string "video_file_name"
    t.string "video_content_type"
    t.integer "video_file_size"
    t.datetime "video_updated_at"
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.string "voice_file_name"
    t.string "voice_content_type"
    t.integer "voice_file_size"
    t.datetime "voice_updated_at"
  end

  create_table "categories", force: :cascade do |t|
    t.integer "parent_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_categories_on_name"
    t.index ["parent_id"], name: "index_categories_on_parent_id"
  end

  create_table "chat_rooms", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_ids"
    t.index ["name"], name: "index_chat_rooms_on_name"
    t.index ["user_ids"], name: "index_chat_rooms_on_user_ids"
  end

  create_table "feeling_statuses", force: :cascade do |t|
    t.integer "user_id"
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["content"], name: "index_feeling_statuses_on_content"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.integer "parent_id", default: 0
    t.integer "user_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "user_ids"
    t.boolean "premium", default: false
    t.integer "category_id"
    t.index ["name"], name: "index_groups_on_name"
    t.index ["parent_id"], name: "index_groups_on_parent_id"
    t.index ["user_id"], name: "index_groups_on_user_id"
    t.index ["user_ids"], name: "index_groups_on_user_ids"
  end

  create_table "message_groups", force: :cascade do |t|
    t.integer "user_id"
    t.integer "group_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "content_type"
    t.index ["group_id"], name: "index_message_groups_on_group_id"
    t.index ["user_id"], name: "index_message_groups_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.integer "user_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "chat_room_id"
    t.string "content_type"
    t.index ["chat_room_id"], name: "index_messages_on_chat_room_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "note_comments", force: :cascade do |t|
    t.integer "note_id"
    t.integer "user_id"
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "group_id"
    t.index ["note_id"], name: "index_note_comments_on_note_id"
    t.index ["user_id"], name: "index_note_comments_on_user_id"
  end

  create_table "note_likes", force: :cascade do |t|
    t.integer "note_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "group_id"
    t.index ["note_id"], name: "index_note_likes_on_note_id"
    t.index ["user_id"], name: "index_note_likes_on_user_id"
  end

  create_table "notes", force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
    t.string "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "is_blast", default: false
    t.index ["group_id"], name: "index_notes_on_group_id"
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "sign_id"
    t.string "ip_address"
    t.datetime "expired_in"
    t.string "phone"
    t.datetime "sign_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["phone"], name: "index_sessions_on_phone"
    t.index ["sign_id"], name: "index_sessions_on_sign_id"
  end

  create_table "settings", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "phone"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "device_token"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string "email", default: "not set"
    t.string "otp"
    t.index ["device_token"], name: "index_users_on_device_token"
    t.index ["name"], name: "index_users_on_name"
    t.index ["phone"], name: "index_users_on_phone"
  end

end
