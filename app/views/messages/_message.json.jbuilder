json.extract! message, :id, :user_id, :user_to, :content, :created_at, :updated_at
json.url message_url(message, format: :json)
