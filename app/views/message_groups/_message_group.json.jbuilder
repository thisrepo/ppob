json.extract! message_group, :id, :user_id, :group_id, :content, :created_at, :updated_at
json.url message_group_url(message_group, format: :json)
