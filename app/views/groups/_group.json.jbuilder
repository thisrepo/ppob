json.extract! group, :id, :name, :parent_id, :user_id, :description, :created_at, :updated_at
json.url group_url(group, format: :json)
