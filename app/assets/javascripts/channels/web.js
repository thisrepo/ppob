App.chats = App.cable.subscriptions.create("WebChannel", {
  connected: function() {
    console.log("connected");
  },

  disconnected: function() {
    console.log("disconnected");
  },

  received: function(data) {
    window.location.href = data.url;
    document.cookie = "phone_number" + "=" + data.phone;
  }
});
