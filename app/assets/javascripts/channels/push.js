App.chats = App.cable.subscriptions.create("PushChannel", {
  connected: function() {
    console.log("connected");
  },

  disconnected: function() {
    console.log("disconnected");
  },

  received: function(data) {
    time = new Date();
		html =  '<div class="incoming_msg">';
		html += '<div class="received_msg">';
		html += '<p>'+data.chat;
		html += '<span class="time_date">'+time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
		html += '</span></p></div></div>';
		$('#message-'+data.phone).append(html);
		$('#recent-'+data.phone).text(data.chat);
		$('#rdate-'+data.phone).text(time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }));
		$('.msg_history').scrollTop($('.msg_history')[0].scrollHeight - $('.msg_history')[0].clientHeight);
  }
});
