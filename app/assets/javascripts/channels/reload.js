App.chats = App.cable.subscriptions.create("ReloadChannel", {
  connected: function() {
    console.log("connected");
  },

  disconnected: function() {
    console.log("disconnected");
  },

  received: function(data) {
    time = new Date();
		html =  '<div class="outgoing_msg">';
		html += '<div class="sent_msg">';
		html += '<p>'+data.chat;
		html += '<span class="time_date">'+time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
		html += '</span></p></div></div>';
		$('#message-'+data.target).append(html);
		$('#recent-'+data.target).text(data.chat);
		$('#rdate-'+data.target).text(time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }));
		$('.msg_history').scrollTop($('.msg_history')[0].scrollHeight - $('.msg_history')[0].clientHeight);
  }
});
