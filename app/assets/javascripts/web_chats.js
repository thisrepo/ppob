$(document).ready(function(){
  $(".chat-item").click(function(){
  	var target_phone = $(this).data('target-phone');
    $.ajax({
    	url: '/web/chats/load_room?to='+target_phone,
    	type: 'GET',
    	success: function(response){
    		$('.chat-item').removeClass('active');
    		$('#room-'+target_phone).addClass('active');

		    $('#head-room').html(response.data.head);
    		$('#chat-content').html(response.data.window);

    		$('.msg_history').scrollTop($('.msg_history')[0].scrollHeight - $('.msg_history')[0].clientHeight);
    	},
    	complete: function(){}
    });
  });
});

function send(target){
  $.ajax({
  	url: '/web/chats/publish?to='+target,
  	type: 'POST',
  	data: {
  		content: $('#content-chat').val().replace(/\n/g, "<br>")
  	},
  	success: function(response){
  		if(response.status=='200'){
  			$('#content-chat').val('');
  			time = new Date();
  			html =  '<div class="outgoing_msg">';
  			html += '<div class="sent_msg">';
  			html += '<p>'+response.content;
  			html += '<span class="time_date">'+time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
  			html += '</span></p></div></div>';
  			$('.msg_history').append(html);
  			$('#recent-'+target).text((response.content).replace(/(<([^>]+)>)/ig," "));
  		}else{
  			console.log(response)
  		}
  		$('.msg_history').scrollTop($('.msg_history')[0].scrollHeight - $('.msg_history')[0].clientHeight);
      $('#ta-frame').removeAttr('style');
      $('#content-chat').removeAttr('style');
  	},
  	complete: function(){}
  });
}