class RoomChannel < ApplicationCable::Channel
  def subscribed
    stream_from "room:#{params[:room]}"
  end

  def unsubscribed
    stop_all_streams
  end
end