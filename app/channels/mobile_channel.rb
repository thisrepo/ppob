class MobileChannel < ApplicationCable::Channel
  def subscribed
    stream_from "mobile_channel"
  end

  def unsubscribed
    stop_all_streams
  end
end
