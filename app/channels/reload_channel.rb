class ReloadChannel < ApplicationCable::Channel
  def subscribed
    if phone_identify.present?
      stream_from "reload:#{phone_identify}"
    else
      stop_all_streams
    end
  end

  def unsubscribed
    stop_all_streams
  end
end