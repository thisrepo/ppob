module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :web_version_identify
    identified_by :phone_identify

    def connect
      self.web_version_identify = cookies[:web_version_identify]
      self.phone_identify = cookies[:phone_number]
    end
  end
end
