class WebChannel < ApplicationCable::Channel
  def subscribed
    if web_version_identify.present?
      stream_from "web_version:#{web_version_identify}"
    else
      stop_all_streams
    end
  end

  def unsubscribed
    stop_all_streams
  end
end
