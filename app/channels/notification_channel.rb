class NotificationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "notification:#{params[:phone]}"
  end

  def unsubscribed
    stop_all_streams
  end
end