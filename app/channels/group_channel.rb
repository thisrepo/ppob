class GroupChannel < ApplicationCable::Channel
  def subscribed
    stream_from "group:#{params[:group]}"
  end

  def unsubscribed
    stop_all_streams
  end
end