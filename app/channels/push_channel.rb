class PushChannel < ApplicationCable::Channel
  def subscribed
    if phone_identify.present?
      stream_from "push:#{phone_identify}"
    else
      stop_all_streams
    end
  end

  def unsubscribed
    stop_all_streams
  end
end