class SessionsController < ApplicationController
  def show
    @session = Session.find(params[:id])
    @session = @session.write(params[:phone], request.remote_ip)

    ActionCable.server.broadcast "web_version:#{@session[:data].sign_id}", { 
      url: Setting::BASE_URL + "/web/chats?sign_id=#{@session[:data].sign_id}", phone: params[:phone] }

    render json: { status: 200, message: "Web Version Connected" }
  end
end