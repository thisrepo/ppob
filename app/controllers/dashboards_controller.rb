class DashboardsController < ApplicationController
  before_action :check_web_session, only: [:index]

  def index
    @session = Session.prepare
    @qr_code = QrCode.generate(Setting::BASE_URL + session_path(@session[:data].id), @session[:data].sign_id)

    cookies[:web_version_identify] = @session[:data].sign_id
  end

  def online; end

  def create
    @user = User.find_or_create_by(phone: params[:phone])
    @user.name = params[:name]

    if @user.save
      @session = Session.prepare[:data]
      @session = @session.write(@user.phone, request.remote_ip)
      session[:sign_id] = @session[:data].sign_id
      cookies[:phone_number] = @user.phone
      session[:phone_number] = @user.phone

      redirect_to web_chats_path, notice: "You are online now"
    else
      redirect_to root_path, notice: "Online access failed"
    end
  end

  private
  def access_params
    params.permit(:phone, :name)
  end
end
