class Research::SynchronizationController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
  	head :no_content
    ActionCable.server.broadcast "live_channel", live_params
  end

  private
  def live_params
    params.permit(:type, :from, :to, :sdp, :candidate)
  end
end
