class Research::RtcConnectionsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
  	@random_number = rand(0...10_000)

  	render layout: false
  end

  def synchronization
    head :no_content
    ActionCable.server.broadcast "live_channel", live_params
  end

  def broadcast
    head :no_content
    ActionCable.server.broadcast "live_channel", broadcast_params
  end

  def live_broadcast
    @random_number = rand(0...10_000)

    render layout: false
  end

  def live_streaming
    @random_number = rand(0...10_000)
    
    render layout: false
  end

  private
  def live_params
    params.permit(:type, :from, :to, :sdp, :candidate)
  end

  def broadcast_params
    params.permit(:sdp, :candidate)
  end
end
