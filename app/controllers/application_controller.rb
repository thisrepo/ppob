class ApplicationController < ActionController::Base
  def check_web_session
    if session[:sign_id].present?
      redirect_to web_chats_path
    end
  end
end
