class Api::NotesController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :prepare_user, only: [:index, :create, :story_like, :story_comment]
	before_action :prepare_group, only: [:index, :create, :story_like, :story_comment]

	def index
		@stories = Note.all

		if @stories.present?
			render json: { status: 200, message: "story found", datas: @stories }
		else
			render json: { status: 404, message: "Story not found" }
		end
	end

	def show
		@group = Group.find(params[:id])

		if @group.present?
			datas = {
				profile: @group.profile,
				notes: @group.group_notes,
				blast: @group.blast_note
			}
			render json: { status: 200, message: "story found", datas: datas }
		else
			render json: { status: 404, message: "story not found" }
		end
	end

	def create
		@story = Note.new
		@story.group_id = @group.id
		@story.user_id = @user.id
		@story.content = params[:content]
		@story.is_blast = params[:is_blast]
		if @story.save
      render json: { status: 200, message: "status update success", datas: @story }
		else
      render json: { status: 404, message: "update note failed" }
		end
	end

	def story_like
		@note = Note.find_by(id: params[:note_id])
		if @note.present?
			@likes = NoteLike.find_by(user_id: @user.id)
			if @likes.present?
				@likes.destroy
      	render json: { status: 200, message: "Unlike" }
			else
				likes = NoteLike.new
				likes.note_id = @note.id
				likes.user_id = @user.id
				likes.group_id = @note.group_id
				likes.save
      	render json: { status: 200, message: "Like" }
			end
		else
      render json: { status: 404, message: "Story not found" }
		end
	end

	def story_comment
		likes = NoteComment.new
		likes.user_id = @user.id
		likes.content = params[:content]
		likes.note_id = params[:note_id]
		likes.group_id = params[:group_id]
		if likes.save
  		render json: { status: 200, message: "Comment published" }
  	else
  		render json: { status: 404, message: "Comment not published" }
  	end
	end

	private
		def story_params
			params.permit(:group_id, :user_id, :content, :comment, :like)
		end

		def prepare_user
	    @user = User.find_by(phone: params[:phone])
	  end

	  def prepare_group
	    @group = Group.find_by(id: params[:group_id])
	  end
end