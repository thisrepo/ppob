class Api::GroupsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :prepare_user, only: [:show, :register, :select, :chatroom, :publish]
  before_action :prepare_participant, only: [:register]

  def show
    if @user.present?
      @groups = @user.groups.order(created_at: :desc)

      if @groups.present?
        render json: { status: 200, message: "user groups found", datas: @groups }
      else
        render json: { status: 404, message: "user groups not found" }
      end
    else
      render json: { status: 404, message: "user not found" }
    end
  end

  def select
    @group = @user.groups.find(params[:group_id])

    if @group.present?
      render json: { status: 200, message: "group found", datas: @group }
    else
      render json: { status: 404, message: "group not found" }
    end
  end

  def register
    @user_ids = @users.pluck(:id).sort
    @group    = @user.groups.new(group_params)
    @group.user_ids = "|#{@user_ids.join('|')}|"

    if @group.parent_id > 0
      parent = Group.find(@group.parent_id)

      if parent.present?
        @group.category_id = parent.category_id
      end
    end

    if @group.save
      render json: { status: 200, message: "register group success", datas: @group }
    else
      render json: { status: 404, message: "register group failed" }
    end
  end

  def chatroom
    @target = Group.find(params[:group_id])

    if @target.present?
      datas = {
        group: @target.profile, 
        chats: @target.chats
      }

      render json: { status: 200, message: "group ready", datas: datas }
    else
      render json: { status: 404, message: "group not ready" }
    end
  end

  def publish
    @target = Group.find(params[:group_id])

    if @target.present?
      @message = @target.message_groups.new(chat_params)
      @message.user_id = @user.id

      if @message.save
        ActionCable.server.broadcast "group:#{@target.id}", {
          user: @user.name, phone: @user.phone, chat: params[:content], content_type: params[:content_type] }

        participant = @target.member.pluck(:phone) << @target.user.phone
        participant.each do |phone|
          unless @user.phone == phone
            ActionCable.server.broadcast "notification:#{phone}", {
              title: "Message from #{@target.name}", content: params[:content] }
          end
        end

        render json: { status: 200, message: "chat has been sent", datas: @chat_room }
      else
        render json: { status: 404, message: "chat_room not ready" }
      end
    else
      render json: { status: 404, message: "user target not ready" }
    end
  end

  private
  def chat_params
    params.permit(:content, :content_type)
  end

  def group_params
    params.permit(:parent_id, :name, :description, :participant, :category_id)
  end

  def prepare_user
    @user = User.find_by(phone: params[:id])
  end

  def prepare_participant
    @users = User.where(phone: params[:participant])
  end
end
