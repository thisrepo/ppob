class Api::ChatsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :prepare_user, only: [:publish, :chatroom, :image, :video, :voice, :file]
  before_action :prepare_target, only: [:publish, :chatroom, :image, :video, :voice, :file]

  def chatroom
    if @target.present?
      init_chatroom

      if @chat_room.present?
        datas = {
          user: @target,
          chat_room: @chat_room.profile, 
          chats: @chat_room.chats
        }

        render json: { status: 200, message: "chat_room ready", datas: datas }
      else
        render json: { status: 404, message: "chat_room not ready" }
      end
    else
      render json: { status: 404, message: "user target not ready" }
    end
  end

  def publish
    if @target.present?
      init_chatroom

      if @chat_room.present?
        @message = @chat_room.messages.new(chat_params)
        @message.user_id = @user.id

        if @message.save
          ActionCable.server.broadcast "reload:#{@user.phone}", {
            user: @user.name, phone: @user.phone, chat: params[:content], target: @target.phone }
          ActionCable.server.broadcast "push:#{@target.phone}", {
            user: @user.name, phone: @user.phone, chat: params[:content] }

          ActionCable.server.broadcast "room:#{@chat_room.id}", {
            user: @user.name, phone: @user.phone, chat: params[:content], content_type: params[:content_type] }

          ActionCable.server.broadcast "notification:#{@target.phone}", {
            title: "Chat from #{@user.name}", content: params[:content] }

          render json: { status: 200, message: "chat has been sent", datas: @chat_room }
        else
          render json: { status: 404, message: "chat failed to send" }
        end
      else
        render json: { status: 404, message: "chat_room not ready" }
      end
    else
      render json: { status: 404, message: "user target not ready" }
    end
  end

  def file
    if @target.present?
      init_chatroom

      if @chat_room.present?
        content_type = 'file'
        @message = @chat_room.messages.create
        @message.user_id = @user.id
        @message.content_type = content_type

        if @message.save
          @message.attachments.create(file: params[:file])

          render json: { status: 200, message: "chat has been sent", datas: @chat_room }
        else
          render json: { status: 404, message: "chat failed to send" }
        end
      else
        render json: { status: 404, message: "chat_room not ready" }
      end
    else
      render json: { status: 404, message: "user target not ready" }
    end
  end

  def image
    if @target.present?
      init_chatroom

      if @chat_room.present?
        content_type = 'image'
        @message = @chat_room.messages.create
        @message.user_id = @user.id
        @message.content_type = content_type

        if @message.save
          @message.attachments.create(image: params[:file])

          render json: { status: 200, message: "chat has been sent", datas: @chat_room }
        else
          render json: { status: 404, message: "chat failed to send" }
        end
      else
        render json: { status: 404, message: "chat_room not ready" }
      end
    else
      render json: { status: 404, message: "user target not ready" }
    end
  end

  def video
    if @target.present?
      init_chatroom

      if @chat_room.present?
        content_type = 'video'
        @message = @chat_room.messages.create
        @message.user_id = @user.id
        @message.content_type = content_type

        if @message.save
          @message.attachments.create(video: params[:file])

          render json: { status: 200, message: "chat has been sent", datas: @chat_room }
        else
          render json: { status: 404, message: "chat failed to send" }
        end
      else
        render json: { status: 404, message: "chat_room not ready" }
      end
    else
      render json: { status: 404, message: "user target not ready" }
    end
  end

  def voice
    if @target.present?
      init_chatroom

      if @chat_room.present?
        content_type = 'voice'
        @message = @chat_room.messages.create
        @message.user_id = @user.id
        @message.content_type = content_type

        if @message.save
          @message.attachments.create(voice: params[:file])

          render json: { status: 200, message: "chat has been sent", datas: @chat_room }
        else
          render json: { status: 404, message: "chat failed to send" }
        end
      else
        render json: { status: 404, message: "chat_room not ready" }
      end
    else
      render json: { status: 404, message: "user target not ready" }
    end
  end

  private
  def chat_params
    params.permit(:content, :content_type)
  end

  def prepare_user
    @user = User.find_by(phone: params[:id])
  end

  def prepare_target
    @target = User.find_by(phone: params[:target])
  end

  def init_chatroom
    unless @user.id == @target.id
      @user_ids  = [@user.id, @target.id].sort
      @chat_room = ChatRoom.find_or_create_by(user_ids: "|#{@user_ids.join('|')}|") do |chat_room|
        chat_room.name = "room_#{@user_ids.join}:#{SecureRandom.hex(3)}"
      end
    end
  end
end