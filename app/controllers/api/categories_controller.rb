class Api::CategoriesController < ApplicationController
	skip_before_action :verify_authenticity_token
	
  def index
    @categories = Category.all.order(:parent_id)

    if @categories.present?
      render json: { status: 200, message: "all category found", datas: @categories }
    else
      render json: { status: 404, message: "all category not found" }
    end
  end

  def main_category
    @categories = Category.where("parent_id < 1 OR parent_id IS NULL").order(:name)

    if @categories.present?
      render json: { status: 200, message: "all category found", datas: @categories }
    else
      render json: { status: 404, message: "all category not found" }
    end
  end
end
