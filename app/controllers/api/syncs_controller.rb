class Api::SyncsController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def call
  	head :no_content
    ActionCable.server.broadcast "call_channel", call_params
  end

  private
  def call_params
  	params.permit(:type, :from, :to, :sdp, :candidate)
  end
end
