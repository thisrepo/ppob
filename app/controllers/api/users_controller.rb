class Api::UsersController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :prepare_user, only: [:show, :device_token, :avatar, :status, :contact, :otp] 

  def index
    @users = User.all.pluck(:phone)

    if @users.present?
      render json: { status: 200, message: "all user found", datas: @users }
    else
      render json: { status: 404, message: "all user not found" }
    end
  end

  def show
    if @user.present?
      datas = { 
        user: @user.profile,
        groups: @user.chat_groups,
        chat_rooms: @user.chat_rooms }
      
      render json: { status: 200, message: "user found", datas: datas }
    else
      render json: { status: 404, message: "user not found" }
    end
  end

  def member
    @users = User.where(phone: params[:contacts])

    if @users.present?
      render json: { status: 200, message: "user found", datas: @users.pluck(:phone) }
    else
      render json: { status: 404, message: "user not found" }
    end
  end

  def register
    @user = User.find_or_create_by(phone: params[:id])
    @user.name = params[:name]
    @user.otp = rand.to_s[2..7]

    if @user.save
      # SmsGateway.publish("+12053410492", "+62#{@user.phone.to_i}", "Your OTP number #{@user.otp}")
      render json: { status: 200, message: "user registered" }
    else
      render json: { status: 404, message: "user register failed" }
    end
  end

  def avatar
    @user.avatar = params[:file]

    if @user.save
      render json: { status: 200, message: "avatar upload success", avatar: Setting::BASE_URL + @user.avatar.url(:thumb2) }
    else
      render json: { status: 404, message: "avatar upload failed" }
    end
  end

  def device_token
    @exitsting_token = User.where(device_token: params[:token])
    @exitsting_token.update_all(device_token: nil)

    if @user.update(device_token: params[:token])
      render json: { status: 200, message: "token registered" }
    else
      render json: { status: 404, message: "token register failed" }
    end
  end

  def status
    @status = @user.feeling_statuses.new
    @status.content = params[:content]

    if @status.save
      render json: { status: 200, message: "status update success", datas: @status }
    else
      render json: { status: 404, message: "status update failed" }
    end
  end

  def contact
    @contacts = @user.contacts

    if @contacts.present?
      render json: { status: 200, message: "contact found", datas: @contacts }
    else
      render json: { status: 404, message: "contact not found" }
    end
  end

  def resend
    @user = User.find_by(phone: params[:id])
    @user.otp = rand.to_s[2..7]
    if @user.save
      SmsGateway.publish("+12053410492", "+62#{@user.phone.to_i}", "Your new number #{@user.otp}")
      render json: { status: 200, message: "OTP Has been resend" }
    else
      render json: { status: 404, message: "Resend OTP Failed" }
    end
  end

  def otp
    if @user.otp.present?
      render json: { status: 200, message: "OTP Found", otp: @user.otp }
    else
      render json: { status: 404, message: "OTP Not found" }
    end
  end

  private
  def prepare_user
    @user = User.find_by(phone: params[:id])
  end
end
