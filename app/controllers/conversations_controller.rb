class ConversationsController < ApplicationController
  before_action :set_user

  def index
    @recent   = JSON.parse(@user.recent_chat.to_json)
    @users    = JSON.parse(@user.contact_list) if params[:id].present?
    @messages = Message.conversations(@user.id, @user_to.id) if params[:to].present?

    redirect_to conversations_path(id: params[:id], to: @recent.first["phone"]) if params[:to].blank? and @recent.present?
  end

  def create
    @message  = Message.chat(@user, @user_to, chat_params)
    @recent   = JSON.parse(@user.recent_chat)
    @users    = JSON.parse(@user.contact_list)
    @messages = Message.conversations(@user.id, @user_to.id)

    respond_to do |format|
      format.html { redirect_to conversations_path(id: params[:id], to: params[:to]), notice: "Select User" }
      format.js { render "reload.js.erb" }
    end
  end

  private
  def set_user
    if params[:id].present?
      interaction = User.from_and_to(chat_params)

      if interaction.present?
        @user = interaction.find_by(phone: params[:id])

        if interaction.count > 1
          @user_to = interaction.find_by(phone: params[:to])

          cookies.delete :first_user
          cookies.delete :second_user
          cookies[:first_user] = @user.id
          cookies[:second_user] = @user_to.id
        end
      end
    else
      redirect_to users_path
    end
  end

  def chat_params
    params.permit(:id, :to, :content)
  end
end
