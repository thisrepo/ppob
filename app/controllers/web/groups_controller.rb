class Web::GroupsController < ApplicationController
	skip_before_action :verify_authenticity_token
  before_action :prepare_sign
  before_action :prepare_user, only: [:index, :load_room, :publish]
  before_action :prepare_target, only: [:index, :load_room, :publish]

  def index
    if @user.present?
      @datas = {
        user: @user.profile,
        chat_rooms: @user.chat_rooms,
        groups: @user.chat_groups }
    end
  end

  def load_room
    if @target.present?
      init_chatroom
      if @chat_room.present?
        @room = {
          user: @target.profile,
          profile: @chat_room.profile, 
          chats: @chat_room.chats
        }
      end
    end
    render json: { data: {
        head:   (render_to_string 'web/partial/_head_room', :layout => false),
        window: (render_to_string 'web/partial/_chat_window', :layout => false)
      }
    }
  end

  def publish
    if @target.present?
      init_chatroom

      if @chat_room.present?
        @message = @chat_room.messages.new(chat_params)
        @message.user_id = @user.id

        if @message.save
          ActionCable.server.broadcast "push:#{@target.phone}", {
            user: @user.name, phone: @user.phone, chat: params[:content] }

          ActionCable.server.broadcast "room:#{@chat_room.id}", {
            user: @user.name, phone: @user.phone, chat: params[:content] }

          ActionCable.server.broadcast "notification:#{@target.phone}", {
            title: "Chat from #{@user.name}", content: params[:content] }

          render json: { status: 200, message: "chat has been sent", datas: @chat_room, content: params[:content] }
        else
          render json: { status: 404, message: "chat failed to send" }
        end
      else
        render json: { status: 404, message: "chat_room not ready" }
      end
    else
      render json: { status: 404, message: "user target not ready" }
    end
  end

  private
  def chat_params
    params.permit(:content)
  end

  def prepare_sign
    if session[:sign_id].present?
      @sign_id = session[:sign_id]
    else
      @sign_id = params[:sign_id]
      session[:sign_id] = params[:sign_id]
    end
  end

  def prepare_user
    session = Session.find_by(sign_id: @sign_id)
    @user = User.find_by(phone: session.phone)
  end

  def prepare_target
    if params[:to].present?
      @target = User.find_by(phone: params[:to])
    end
  end

  def init_chatroom
    unless @user.id == @target.id
      @user_ids  = [@user.id, @target.id].sort
      @chat_room = ChatRoom.find_or_create_by(user_ids: "|#{@user_ids.join('|')}|") do |chat_room|
        chat_room.name = "room_#{@user_ids.join}:#{SecureRandom.hex(3)}"
      end
    end
  end
end
