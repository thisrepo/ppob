class Admin::CategoriesController < ApplicationController
  def index
    @categories = Category.all.order(:parent_id)
  end

  def create
  	params[:parent_id] = 0 if params[:parent_id].nil?
  	@category = Category.find_or_create_by(name: params[:name], parent_id: params[:parent_id])

  	redirect_to admin_categories_path
  end
end
