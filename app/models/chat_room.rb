class ChatRoom < ApplicationRecord
  include Rails.application.routes.url_helpers
  
  has_many :messages

  def partner(user)
    user_ids = self.user_ids.split("|").reject { |c| c.empty? }
    partner = User.where(id: user_ids).where.not(id: user.id).first

    return {
      name: partner.name,
      phone: partner.phone,
      avatar: partner.avatar.url(:thumb2),
      status: partner.read_status }
  end

  def profile
    return {
      id: self.id,
      name: self.name,
      created_at: self.created_at.strftime("%A, %b %e, %H:%M") }
  end

  def recent(user)
    chat = self.messages.last
    contact = self.partner(user)

    data = { 
      user: contact[:name],
      phone: contact[:phone],
      status: contact[:status],
      avatar: Setting::BASE_URL + contact[:avatar], 
      content: chat&.content,
      content_type: chat&.content_type,
      created_at: chat&.created_at&.strftime("%Y-%m-%d %H:%M:%S") }

    return data
  end

  def chats
    messages = []

    self.messages.each do |message|
      unless message.attachments.present?
        messages << { 
          id: message.id,
          user: message.user.name,
          phone: message.user.phone,
          content: message.content,
          content_type: message.content_type,
          created_at: message.created_at.strftime("%Y-%m-%d %H:%M:%S"),
          attachment: false
        }
      else
        attachment_type = message.attachments.first
        unless attachment_type.image.url.include?("missing")
          attach_type = "image"
          mime_type = message.attachments.first.image_content_type
          attachment = message.attachments.first.image.url(:original)
        end

        unless attachment_type.video.url.include?("missing")
          attach_type = "video"
          mime_type = message.attachments.first.video_content_type
          attachment = message.attachments.first.video.url(:original)
        end

        unless attachment_type.voice.url.include?("missing")
          attach_type = "voice"
          mime_type = message.attachments.first.voice_content_type
          attachment = message.attachments.first.voice.url(:original)
        end

        unless attachment_type.file.url.include?("missing")
          attach_type = "file"
          mime_type = message.attachments.first.file_content_type
          attachment = message.attachments.first.file.url(:original)
        end

        messages << { 
          id: message.id,
          user: message.user.name,
          phone: message.user.phone,
          content: message.content,
          content_type: message.content_type,
          mime_type: mime_type,
          attachment: Setting::BASE_URL + attachment,
          attachment_name: message.attachments.first.file_file_name,
          attachment_type: attach_type,
          created_at: message.created_at.strftime("%Y-%m-%d %H:%M:%S")
        }
      end
    end

    return messages
  end
end
