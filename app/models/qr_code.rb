class QrCode < ApplicationRecord
  class << self
    def generate(url, session_id)
      qrcode = RQRCode::QRCode.new(url)
      qr_png = qrcode.as_png(
        resize_gte_to: false,
        resize_exactly_to: false,
        fill: 'white',
        color: 'black',
        size: 300,
        border_modules: 4,
        module_px_size: 6 )
      directory = Rails.root.join('public', 'qrcode', session_id)

      Dir.mkdir(directory) unless Dir.exist?(directory)
      File.open(directory.join("qrcode.png"), 'wb') do |file|
        file.write(qr_png)
      end

      path = "/qrcode/#{session_id}/qrcode.png"

      return { status: 200, message: "generate qr success", data: path }
    rescue => error
    	return { status: 404, message: "generate qr failed", error: error }
    end
  end
end