class Session < ApplicationRecord
  def write(phone, ip)
    sign = self

    sign.ip_address = ip
    sign.phone = phone
    sign.sign_at = Time.now

    if sign.save
      return { status: 200, message: "write session success", data: sign }
    else
      return { status: 404, message: "write session failed" }
    end
  end

  class << self
    def prepare
      sign = Session.new
      sign.sign_id = SecureRandom.hex(5)
      sign.expired_in = Time.now + 1.day

      if sign.save
        return { status: 200, message: "prepare session success", data: sign }
      else
        return { status: 404, message: "prepare session failed" }
      end
    end
  end
end
