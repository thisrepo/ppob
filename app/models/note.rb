class Note < ApplicationRecord
	belongs_to :group
	belongs_to :user
	has_many :note_likes
	has_many :note_comments

end