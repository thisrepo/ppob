class SmsGateway < ApplicationRecord
	ACCOUNT_SID = 'ACe0ce2250b473cd96bc450a9cc2809b1c'
  AUTH_TOKEN = '24e1575f0189ef7ed7466cc303e12a90'

  class << self
  	def publish(from, to, body)
  		init = Twilio::REST::Client.new(SmsGateway::ACCOUNT_SID, SmsGateway::AUTH_TOKEN)
  		init.messages.create(from: from, body: body, to: to)
  	end
  end
end