class Attachment < ApplicationRecord
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/missing.png"
  has_attached_file :video
  has_attached_file :voice
  has_attached_file :file

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates_attachment_content_type :voice, content_type: /.*/

  belongs_to :message
end
