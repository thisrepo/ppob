class Message < ApplicationRecord
  belongs_to :user
  belongs_to :chat_room
  has_many :attachments

  # after_save :reload_page

  def reload_page
    user = User.find(self.user_to)
    recent   = JSON.parse(user.recent_chat)
    users    = JSON.parse(user.contact_list)
    messages = Message.conversations(self.user_id, self.user_to)
    
    html_recent = ApplicationController.render partial: "conversations/recent", locals: {recent: recent, user: self.user}, formats: [:html]
    html_messages = ApplicationController.render partial: "conversations/messages", locals: {messages: messages, user: user}, formats: [:html]

    ActionCable.server.broadcast "from_#{self.user_to}:to_#{self.user_id}", {html: {recent: html_recent, messages: html_messages}}
  end

  class << self
    def chat(user, user_to, params)
      message = user.messages.new
      message.user_to = user_to.id
      message.content = params[:content]

      if message.save
        return { data: message, message: "success" }
      else
        return { data: message, message: message.errors }
      end
    end

    def conversations(from, to)
      where("(user_id = #{from} AND user_to = #{to}) OR (user_id = #{to} AND user_to = #{from})").order(:created_at)
    end
  end
end
