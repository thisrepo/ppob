class Category < ApplicationRecord
  has_many :groups

  def parent
    if self.parent_id.to_i > 0
      parent = Category.find(self.parent_id)

      return parent.name
    else
      return "Main Category"
    end
  end
end
