class User < ApplicationRecord
  has_attached_file :avatar, styles: { thumb2: "180x180#", thumb: "180x180>" }, default_url: "/default-avatar.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  has_many :messages
  has_many :groups
  has_many :message_groups
  has_many :feeling_statuses
  has_many :notes
  has_many :note_likes
  has_many :note_comments

  def profile
    return {
      id: self.id,
      name: self.name,
      phone: self.phone,
      email: self.email,
      status: self.read_status,
      avatar: Setting::BASE_URL + self.avatar.url(:thumb2),
      point: 0,
      coin: 0,
      connection: 0,
      created_at: self.created_at.strftime("%Y-%m-%d %H:%M:%S") }
  end

  def chat_rooms
    arr_rooms = []
    chat_rooms  = ChatRoom.where("user_ids LIKE ?", "%|#{self.id}|%").order(created_at: :desc)
    chat_rooms.each do |room|
      arr_rooms << { 
        id: room.id, 
        name: room.name, 
        created_at: room.created_at.strftime("%Y-%m-%d %H:%M:%S"),
        recent_chat: room.recent(self) }
    end

    return arr_rooms
  end

  def chat_groups
    arr_groups = []
    chat_groups  = Group.where("user_ids LIKE ? OR user_id = ?", "%|#{self.id}|%", self.id).order(created_at: :desc)
    chat_groups.each do |group|
      arr_groups << { 
        id: group.id,
        name: group.name,
        parent: group.parent&.name,
        main_parent: group.parent&.parent&.name,
        owner: group.user.name,
        phone: group.user.phone,
        description: group.description,
        member: group.user_ids.split("|").reject { |c| c.empty? }.uniq.count + 1,
        category: group.category&.name,
        created_at: group.created_at.strftime("%Y-%m-%d %H:%M:%S") }
    end

    return arr_groups
  end

  def contact_list
    contact = User.where.not(id: self.id).to_json

    return contact
  end

  def read_status
    return self.feeling_statuses.last.content
  rescue
    return "I'm using ChatU"
  end

  def recent_chat
    recent = []
    recent_user_id = self.messages.pluck(:user_to).uniq
    recent_user = User.where(id: recent_user_id)

    recent_user.each do |user|
      hash_user = JSON.parse(user.to_json)
      recent_chat = Message.conversations(self.id, user.id).last
      recent << hash_user.merge({
        "recent" => {
          "content" => recent_chat.content, 
          "created_at" => recent_chat.created_at
        }
      })
    end

    sort_recent = recent.sort { |a,b| a['recent']['created_at'] <=> b['recent']['created_at'] }.reverse!

    return sort_recent
  end

  def recent
    recent = []
    recent_user_id = self.messages.pluck(:user_to).uniq
    recent_user = User.where(id: recent_user_id)

    recent_user.each do |user|
      hash_user = JSON.parse(user.to_json)
      recent_chat = Message.conversations(self.id, user.id).last
      recent << hash_user.merge({
        "recent_message" => {
          "content" => recent_chat.content, 
          "content_type" => recent_chat.content_type,
          "created_at" => recent_chat.created_at
        }
      })
    end

    sort_recent = recent.sort { |a,b| a['recent_message']['created_at'] <=> b['recent_message']['created_at'] }.reverse!

    return sort_recent
  end

  def contacts
    contacts = []
    chat_rooms  = ChatRoom.where("user_ids LIKE ?", "%|#{self.id}|%").order(created_at: :desc)
    chat_rooms.each do |room|
      contact = room.partner(self)
      contacts << { name: contact[:name], phone: contact[:phone] }
    end

    return contacts
  rescue
    return nil
  end

  class << self
    def from_and_to(params)
      where("phone = '#{params[:id]}' OR phone = '#{params[:to]}'")
    end
  end
end
