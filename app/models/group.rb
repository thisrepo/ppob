class Group < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :message_groups
  has_many :notes

  def parent
    if self.parent_id.to_i > 0
      parent = Group.find(self.parent_id)

      return parent
    else
      return nil
    end
  rescue
    return nil
  end

  def member
  	user_ids = self.user_ids.split("|").reject { |c| c.empty? }
    member = User.where(id: user_ids)

    return member
  end

  def profile
    return {
      id: self.id,
      name: self.name,
      parent: self.parent&.name,
      main_parent: self.parent&.parent&.name,
      description: self.description,
      created_at: self.created_at.strftime("%A, %b %e, %H:%M") }
  end

  def chats
    messages = []

    self.message_groups.each do |message|
      messages << { 
        id: message.id,
        user: message.user.name,
        phone: message.user.phone,
        content: message.content,
        content_type: message.content_type,
        created_at: message.created_at.strftime("%Y-%m-%d %H:%M:%S")
      }
    end

    return messages
  end

  def group_notes
    response = []
    self.notes.each do |tmp|
      response << {
        id: tmp.id,
        user_id: tmp.user_id,
        group_id: tmp.group_id,
        username: tmp.user.name,
        is_blast: tmp.is_blast,
        like: tmp.note_likes.count,
        content: tmp.content,
        comment: tmp.note_comments,
        created_at: tmp.created_at.strftime("%Y-%m-%d %H:%M"),
        updated_at: tmp.updated_at.strftime("%Y-%m-%d %H:%M")
      }
    end
    return response
  end

  def blast_note
    @is_blast = Note.find_by(is_blast: true)
      if @is_blast.present?
        response = []
        self.notes.each do |tmp|
          response << {
            id: tmp.id,
            user_id: tmp.user_id,
            group_id: tmp.group_id,
            username: tmp.user.name,
            is_blast: tmp.is_blast,
            like: tmp.note_likes.count,
            content: tmp.content,
            comment: tmp.note_comments,
            created_at: tmp.created_at.strftime("%Y-%m-%d %H:%M"),
            updated_at: tmp.updated_at.strftime("%Y-%m-%d %H:%M")
          }
        end
      end
    return @is_blast
  end

end
