class Firebase < ApplicationRecord
  FCM_URL = "https://fcm.googleapis.com/fcm/send"
  SERVER_KEY = "AAAAvuZnqB8:APA91bEVr3XKbf2FKLhSVDgRYUPmMoW6IBmP06xLI-BP2ndPTFutckTPKOvhOMT2xiIDZ6a_bh7yB34We-6nxDboZjwO8Dpn0_KA-nbWXmNyVjhZEKm4uUcWytP4_9auAHCrW-jdP9cC"
  LEGACY_SERVER_KEY = "AIzaSyCGLtWQ9qV5yYf1-q6grQ7asjQaCVUBhMg"
  SENDER_ID = "819909339167"

  # ===== This is example to use Firebase
  # parameter = { 
  #   body: content, 
  #   title: user.name, 
  #   content: content, 
  #   user: user.name,
  #   phone: user.phone,
  #   push_type: "chat",
  #   room_id: @chat_room.id }
  # broadcast = Firebase.init(@target, parameter)

  class << self
    def init(user, params)
      room = params[:room_id]
      data = {
        "title" => params[:title],
        "content" => params[:content],
        "body" => params[:body],
        "user" => params[:user],
        "phone" => params[:phone]
      }

      case params[:push_type]
      when "chat"
        data = data.merge({"room_id" => room})
      when "group"
        data = data.merge({"group_id" => room})
      end
        
      parameter = Firebase.prepare_object(user, params[:body], params[:title], data)
      broadcast = Firebase.push_notification(parameter.to_json)
    end

    def prepare_object(user=Firebase::TEST_CLIENT_TOKEN, body, title, datas)
      return { 
        to: user.device_token,
        notification: {
          body: body,
          title: title },
        data: datas
      }
    end

    def push_notification(parameter)
      conn = Faraday.new(url: Firebase::FCM_URL) do |req|
        req.headers['Authorization'] = "key=#{Firebase::LEGACY_SERVER_KEY}"
        req.headers['Content-Type'] = "application/json"
        req.adapter  Faraday.default_adapter
      end

      conn.post Firebase::FCM_URL, parameter
    end
  end
end
